(function($){

	window.wpUploaderInit.runtimes = 'html5,html4';

	$.fn.serializeObject = function(){
		var self = this,
			json = {},
			push_counters = {},
			patterns = {
				"validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
				"key":      /[a-zA-Z0-9_]+|(?=\[\])/g,
				"push":     /^$/,
				"fixed":    /^\d+$/,
				"named":    /^[a-zA-Z0-9_]+$/
			};


		this.build = function(base, key, value){
			base[key] = value;
			return base;
		};

		this.push_counter = function(key){
			if(push_counters[key] === undefined){
				push_counters[key] = 0;
			}
			return push_counters[key]++;
		};

		$.each($(this).serializeArray(), function(){
			// skip invalid keys
			if(!patterns.validate.test(this.name)){
				return;
			}

			var k,
				keys = this.name.match(patterns.key),
				merge = this.value,
				reverse_key = this.name;

			while((k = keys.pop()) !== undefined){

				// adjust reverse_key
				reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

				// push
				if(k.match(patterns.push)){
					merge = self.build([], self.push_counter(reverse_key), merge);
				}

				// fixed
				else if(k.match(patterns.fixed)){
					merge = self.build([], k, merge);
				}

				// named
				else if(k.match(patterns.named)){
					merge = self.build({}, k, merge);
				}
			}

			json = $.extend(true, json, merge);
		});

		return json;
	};

	var fileForm = $('#file-form');

	fileForm.on('change', 'input[name^="tax_input"]', function(){
		var taxonomies = $('#file-form input[name^="tax_input"]').serializeObject().tax_input;

		fileForm.trigger('change:taxonomies', taxonomies);

		if (_.isUndefined(taxonomies)) {
			return;
		}

		_(taxonomies).each(function(ids, taxonomy) {
			window.wpUploaderInit.multipart_params['tax-' + taxonomy] = ids.join(',');
		});
	});


	$(function() {
		window.post_id = 0;

		uploader.bind('BeforeUpload', function(up, file) {
			if ( 'function' === typeof window.uploadSuccess ) {
				window.uploadSuccess = jQuery.noop;
			}

			$('#plupload-upload-ui, .upload-flash-bypass').hide();
		});

		uploader.bind('UploadComplete', function(up, files) {
			// $('#media-items').empty();
			// console.log(files);
		});

		uploader.bind('FileUploaded', function(up, file, response) {
			var attachmentId = response.response;

			$.get('/wp-admin/admin-ajax.php', {action: 'get_parent_post', id: attachmentId}, function(response) {
				var item = jQuery('#media-item-' + file.id);
				// console.log(file);
				item.html(response.data.html);
			});
		});

	});

})(jQuery);