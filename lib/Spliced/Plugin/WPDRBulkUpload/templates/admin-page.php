<?php if ( ! current_user_can( 'upload_files' ) )
	wp_die( __( 'You do not have permission to upload files.' ) );

	$form_class = 'media-upload-form type-form validate';

	if ( get_user_setting( 'uploader' ) || isset( $_GET['browser-uploader'] ) )
		$form_class .= ' html-uploader';

	$post_id = -1;
?><div class="wrap">
	<?php screen_icon(); ?>
	<h2>Bulk Upload</h2>

	<form enctype="multipart/form-data" method="post" action="<?php esc_attr_e( $page_path ); ?>" class="<?php esc_attr_e( $form_class ) ?>" id="file-form">

		<?php media_upload_form(); ?>

		<input type="hidden" name="post_id" id="post_id" value="<?php esc_attr_e( $post_id ) ?>" />
		<input type="hidden" name="spliced-wpdr-bulk-upload" id="" value="bulk" />
		<?php wp_nonce_field( 'spliced-wpdr-bulkupload' ); ?>

		<div id="media-items" class="hide-if-no-js"></div>
	</form>
</div>


