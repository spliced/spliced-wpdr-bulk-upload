<?php
namespace Spliced\Plugin\WPDRBulkUpload;

/**
 * Class Spliced\Plugin\WPDRBulkUpload
 */
class Plugin {
	const ADMIN_PAGE_SLUG = 'spliced-wpdr-bulk-upload';

	static $_hooker;

	static public function bootstrap( $hooker = null ) {
		if ( $hooker ) {
			if ( ! method_exists( $hooker, 'hook' ) )
				throw new \BadMethodCallException( 'Class ' . get_class( $hooker ) . ' has no hook() method.', 1 );

			self::$_hooker = $hooker;
			self::$_hooker->hook( __CLASS__, 'spliced-wpdr-bulk-upload' );
		} else {
			throw new \BadMethodCallException( 'Hooking class for ' . __CLASS__ . ' not specified.' , 1 );
		}

		register_activation_hook( __FILE__, array( __CLASS__, 'activation' ) );

		add_action( 'admin_print_scripts-document_page_' . self::ADMIN_PAGE_SLUG, array( __CLASS__, 'enqueue_admin_page_scripts' ) );
	}

	function activation() {}

	/**
	 * Add our admin page
	 */
	public function action_admin_menu() {
		$handle = add_submenu_page(
			'edit.php?post_type=document',
			__( 'Upload Multiple Documents', 'spliced-wpdr-bulk-upload' ),
			__( 'Bulk Upload', 'spliced-wpdr-bulk-upload' ),
			'publish_documents',
			self::ADMIN_PAGE_SLUG,
			array( __CLASS__, 'options_page_bulk_upload' )
		);
	}

	/**
	 * Load the admin page content
	 */
	public function options_page_bulk_upload() {
		$page_path = admin_url( 'edit.php?post_type=document&page=' . self::ADMIN_PAGE_SLUG );

		include_once __DIR__ . '/templates/admin-page.php';
	}

	public function enqueue_admin_page_scripts() {
		wp_enqueue_script( 'plupload-handlers' );
		wp_dequeue_script( 'wp_document_revisions' );
		wp_enqueue_script( 'spliced-wpdr-bulk-upload', plugins_url( 'js/admin.js', __FILE__ ), array( 'plupload-handlers', 'jquery', 'underscore' ), microtime(), true );
	}

	public function action_admin_init() {
		if ( class_exists( '\Document_Revisions_Admin' ) ) {
			remove_filter( 'media_meta', array( \Document_Revisions_Admin::$instance, 'media_meta_hack' ), 10, 1 );
		}
	}

	public function filter_upload_post_params( $params ) {
		if ( self::is_bulk_upload_screen() ) {
			// Add a flag to the POST params so we know that we have
			// have sent the upload from the bulk uploader.
			$params['spliced-wpdr-bulk-upload'] = 'bulk';
		}

		return $params;
	}

	/**
	 * Insert our taxonomy checkboxes into the upload form
	 */
	public function action_post_plupload_upload_ui() {
		static $wp_hook_override = 'pre-plupload-upload-ui';

		if ( self::is_bulk_upload_screen() ) {
			$taxonomies = get_object_taxonomies( 'document', 'names' );
			$terms = get_terms( $taxonomies, array( 'hide_empty' => 0 ) );
			?>
			<div class="wpdr-bulk-upload--taxonomies">
				<h3><?php _e( 'Select taxonomies for the documents. You must choose <strong>at least one</strong> category.</h3>', 'spliced-wpdr-bulk-upload' ) ?>
				<?php foreach ( $taxonomies as $taxonomy ) : ?>
				<div class="wpdr-bulk-upload--taxonomy" style="display: inline-block; width: 33%; vertical-align:top">
					<p><?php esc_html_e( get_taxonomy( $taxonomy )->labels->name ) ?></p>
					<ul class="wpdr-bulk-upload--taxonomy-terms wpdr-bulk-upload--taxonomy-<?php esc_attr_e( $taxonomy ) ?>-terms"><?php wp_terms_checklist( 0, array( 'taxonomy' => $taxonomy ) ) ?></ul>
				</div>
				<?php endforeach ?>
			</div>
			<?php
		}
	}

	/**
	 * Link up our document post and attachment post after they have been uploded
	 * @param  int $attachment_id The attachment post ID
	 */
	public function action_add_attachment( $attachment_id ) {
		$attachment_id = (int) $attachment_id;
		if ( self::is_bulk_upload_request() ) {
			$attachment = get_post( $attachment_id );
			//build post array and insert post
			$post = array(
				'post_title' => $attachment->post_title,
				'post_status' => 'private',
				'post_author' => wp_get_current_user()->ID,
				'post_content' => $attachment_id,
				'post_excerpt' => 'Initial upload',
				'post_type' => 'document',
			);
			$postID = wp_insert_post( $post );

			wp_update_post(
				array(
					'ID' => $attachment_id,
					'post_parent' => $postID,
				)
			);

			$taxonomies = array();
			foreach ( $_REQUEST as $key => $value ) {
				if ( strpos( $key, 'tax-' ) !== false ) {
					$taxonomies[str_replace( 'tax-', '', $key )] = explode( ',', $value );
				}
			}

			foreach ( $taxonomies as $taxonomy => $term_ids ) {
				wp_set_post_terms( $postID, $term_ids, $taxonomy, true );
			}
		}
	}

	/**
	 * Ajax callback to display uploaded post data
	 */
	public function action_wp_ajax_get_parent_post( ) {
		$attachment_id = (int) $_REQUEST['id'];
		$attachment    = get_post( $attachment_id );
		$post          = get_post( $attachment->post_parent );

		if ( ! user_can( wp_get_current_user(), 'read_documents' ) ) {
			wp_die();
		}

		global $wpdr;
		$data = array(
			'post' => $wpdr->get_latest_revision( $post->ID ),
			'permalink' => get_permalink( $post->ID ),
		);

		if ( current_user_can( 'edit_private_documents' ) ) {
			$data['edit_link'] = get_edit_post_link( $post->ID );
			$edit_html = " | <a href='{$data['edit_link']}' target='_blank'>Edit</a>";
		}

		$data['html'] = "<div class='inner' style='padding:1em'>Upload complete: " . get_the_title( $post->ID ) . " <a href='{$data['permalink']}' target='_blank'>View</a>$edit_html</div>";

		wp_send_json_success( $data );
	}

	private function is_bulk_upload_request() {
		return ! empty( $_REQUEST['spliced-wpdr-bulk-upload'] );
	}

	private function is_bulk_upload_screen() {
		return 'document_page_spliced-wpdr-bulk-upload' === get_current_screen()->id;
	}
}